<?php
    include(__DIR__."/../classes/playlist.php");
    include(__DIR__."/../classes/DB.php");
    session_start();

    $url = $_POST['url'];
    $id_utilisateurs = $_SESSION["id_utilisateurs"];

    function check_substr($str){
        if (preg_match('/^http[s]?:\/\/www\.youtube\.com\/watch\?v=.*$/',$str)){
            preg_match('/^.*=([\w]*)&?.*$/',$str,$matches);
            return $matches[1];
        }
        else if (preg_match('/http[s]?:\/\/youtu.be\/.*$/',$str)){
            preg_match('/^.*be\/([\w]*)\?.*$/',$str,$matches);
            return $matches;
        }
        else{
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
     }
     $urlRetrieve = check_substr($url);


    DB::addUrl($urlRetrieve,$id_utilisateurs);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>