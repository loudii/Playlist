<?php
session_start();

include(__DIR__.'/../classes/playlist.php');
include(__DIR__.'/../classes/DB.php');

//      Défini la variable id

$login = $_POST['login'];
$password = $_POST['password'];
$verifyLogin = Db::login($login, $password);

if (empty($verifyLogin)){
    $_SESSION['error_message'] = "Login / password  invalid ";
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}
else{
    $_SESSION["id_utilisateurs"] = $verifyLogin["id"];
    $_SESSION["login"] = $verifyLogin["login"];
    header('Location: ../index1.php');
}


?>