<?php
session_start();

include(__DIR__.'/../classes/playlist.php');
include(__DIR__.'/../classes/DB.php');


// donnees
$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$login = $_POST['login'];
$password = $_POST['password'];

// validation
$empty = empty($lastname) || empty($firstname) || empty($login) || empty($password);
if ($empty){
    $_SESSION['error'] = ' viiiiiiide !! ';
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    return;
}

// enregistrement
Db::signUp($lastname, $firstname, $login, $password);
header('Location: ../indexLogin.php');


?>