<?php
    include('../classes/playlist.php');
    include('../classes/DB.php');

    //      Défini la variable id
    $id = $_POST['id'];
    //      Suppression de l'url sélectionné grâce a son ID dans la bdd
    Db::deleteUrl($id);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>