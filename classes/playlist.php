<?php
    class Playlist{
        public $url;
        private $id;

        public function __construct($url, $id){
            $this->setUrl($url);
            if (isset($id)) { $this->id = $id; }
        }

        //      Récupère la variable Id
        public function getId(){
            return $this->id;
        }

        //      Récupère la variable url
        public function getUrl(){
            return $this->url;
        }

        //      Permet de modifier la valeur de la variable url
        public function setUrl($valueUrl){
            $this->url = $valueUrl;
        }
    }
?>