<?php

class Db {

    //      Lien entre le fichier et la BDD
    static function connection(){
        $user = 'root';
        $password = 'root';
        $host = 'localhost' ;
        $database = 'Playlist';

        $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $bdd;
    }
    //      Function pour ajouter une URL à la BDD
    static function addUrl($url,$id_user){
        $bdd = self::connection();
        $request = $bdd->prepare("INSERT INTO playlist VALUES(NULL, :url, :id)");
        $request->execute(['url' => $url,'id'=> $id_user]);
    }
    //      Function pour supprimer une URL à la BDD
    static function deleteUrl($id){
        $bdd = self::connection();
        $request = $bdd->prepare('DELETE FROM playlist WHERE id = :id');
        $request->execute(['id' => $id]);
    }
    static function signUp($lastname, $firstname, $login, $password){
        $bdd = self::connection();
        $request = $bdd->prepare("INSERT INTO utilisateurs VALUES(NULL, :lastname, :firstname, :login, :password)");
        $request->execute(['lastname' => $lastname, 'firstname' => $firstname, 'login' => $login, 'password' => $password]);
    }
    static function login($login, $password){
        $bdd = self::connection();
        $request = $bdd->prepare("SELECT id, login FROM utilisateurs WHERE login =:login AND password =:password");
        $request->execute(['login' => $login, 'password' => $password]);
        $request = $request->fetch();
        return $request;
    }
}
?>