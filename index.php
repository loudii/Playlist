<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Orbitron" rel="stylesheet">
    <title>YouMix</title>
</head>
<body>
<?php

    $error = isset($_SESSION['error_message']) ? $_SESSION['error_message'] : '';
    $error1 = isset($_SESSION['error']) ? $_SESSION['error'] : '';

    include(__DIR__.'/classes/playlist.php');
    include(__DIR__.'/classes/DB.php');
?>
    <div class="title">
        <h2>YouMix</h2>
        <img class="imgHP" src="assets/hp.png">
    </div>
    <div class="log_sign">
        <form class="sign_in" method="post" action="forms/sign_up.php">
            <div class="error"><?php echo $error1; ?></div>
            <input type="text" name="firstname" class="firstname" placeholder="Firstname">
            <input type="text" name="lastname" class="lastname" placeholder="Lastname">
            <input type="text" name="login" class="login" placeholder="Login">
            <input type="password" name="password" class="password" placeholder="Password">
            <button type="submit" name="sign_in" class="sign_in">Sign in</button>
        </form>
        <div class="borderR"></div>

        <form class="login"  method="post" action="forms/login.php">
            <div class="error"><?php echo $error; ?></div>
            <input type="text" name="login" class="login" placeholder="Login">
            <input type="password" name="password" class="password" placeholder="Password">
            <button type="submit" class="login">Login</button>
        </form>
    </div>
    <?php session_destroy(); ?>
</body>
</html>