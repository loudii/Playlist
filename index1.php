<?php
session_start();
$loginUser = $_SESSION["login"];
$user = isset($_SESSION['id_utilisateurs']) ? $_SESSION['id_utilisateurs'] : '';
?>

<!DOCTYPE html>
<html lang=fr>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Orbitron" rel="stylesheet">
    <script type="text/javascript" src="player.js"></script>
    <title>YouMix</title>
</head>
<body>
    <?php
        include(__DIR__.'/classes/playlist.php');
        include(__DIR__.'/classes/DB.php');
    ?>
    <div class="navbar">
        <div class="login_user">
            <p class="helloLogin">
                <p class="hello">Hello</p>
                <p class="pseudo"><?php echo $loginUser; ?> !</p>
            </p>
            <div class="borderLogin"></div>
            <a class="logout" href="logout.php">LOGOUT</a>
        </div>
        <div class="title">
            <h1>YouMix</h1>
            <img class="imgHP" src="assets/hp.png">
        </div>
    </div>
    <div class="container">
        <div id="player"></div>
            <div class="music">
                <?php
                    $bdd = Db::connection();
                    $request = $bdd->prepare('SELECT * FROM playlist INNER JOIN utilisateurs ON utilisateurs.id = playlist.id_utilisateurs WHERE utilisateurs.id = :user');
                    $request->execute(['user' => $user]);
                    $urls = $request->fetchAll();
                    $listUrl = [];

                    foreach($urls as $urlArray){
                        $listUrl[] = $urlArray['url'];
                        $url = new Playlist($urlArray['url'], $urlArray['0']);

                        $array = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/videos?part=id,snippet&id='.$url->getUrl().'&key=AIzaSyCU829MgrK3t94MXo6FO-I_V1eOC-FgIDA'));
                        $title = $array->items[0]->snippet->title;
                ?>
                <div class="new_music">
                    <div class="input_playlist">
                            <input class="play" name="play" type="image" src="assets/play.png" data-id="<?php echo $url->getUrl(); ?>"/>
                            <input class="pause" name="pause" type="image" src="assets/pause.png" data-id="<?php echo $url->getUrl(); ?>"/>
                            <div class="urlPlaylist" ><?php echo $title; ?></div>
                            <input id="playerUrl" type="hidden" name="id" value="<?php echo $url->getId(); ?>" />
                        </div>
                        <div class="trash">
                            <form method="post" action="forms/deleteUrl.php">
                                <input name="id" type="hidden" value="<?php echo $url->getId(); ?>" />
                                <input class="delete" name="delete" type="image" src="assets/trash.png" data-id="<?php echo $url->getUrl(); ?>"/>
                            </form>
                        </div>
                    </div>
                <div class="borderB"></div>
                    <?php
                    }
                    ?>
            </div>
    </div>
    <div class="add_playlist">
        <form class="add" method="POST" action="forms/addUrl.php">
            <input type="text" class="add_url" name="url"/>
            <input class="plus" name="add" type="image" src="assets/plus.png"/>
        </form>
    </div>
</body>
<script>
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      var listUrl = <?php echo json_encode($listUrl) ?>;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '318',
          width: '550',
          videoId: listUrl[0],
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      // 4. The API will call this function when the video player is ready.

    function toArray(nodelist) {
        return ([].slice.call(nodelist))
    }
    function onPlayerReady(event) {
        var play = document.querySelectorAll(".play");
        play.forEach(function(button) {
            button.addEventListener("click", function(event){
            player.loadVideoById(this.dataset.id);
            player.playVideo();
            });
        });


        var pause = document.querySelectorAll(".pause");
            pause.forEach(function(button) {
            button.addEventListener("click", function(event){
            player.pauseVideo();
            });
        });
    }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var i = 0;

      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            player.loadVideoById(listUrl[++i]);
        }
      }

      function stopVideo() {
        player.stopVideo();
      }
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>